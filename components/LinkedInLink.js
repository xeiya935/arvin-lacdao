import React from 'react';
import Head from 'next/head';

export default function FacebookLink() {
	return(
		<React.Fragment>
			<Head>
				<script src="https://kit.fontawesome.com/8e97c30ac7.js" crossOrigin="anonymous"></script>
			</Head>
			<i className="fab fa-linkedin footer-link" aria-hidden="true"></i>
		</React.Fragment>
	)
}