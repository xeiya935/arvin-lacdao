import Link from 'next/link';
import { Navbar, Nav, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMicrochip, faLaptopCode, faGraduationCap, faBriefcase, faMobileAlt } from '@fortawesome/free-solid-svg-icons';

export default function NavigationBar() {
	return(
		<Navbar expand="lg" className="fixed-top" collapseOnSelect>
			<Link href="/">
				<Navbar.Brand href="/" className="text-light">Arvin Lacdao</Navbar.Brand>
			</Link>
			<Navbar.Toggle aria-controls="basic-navbar-nav" className="bg-light"/>
				<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Link href="/about" passHref>
						<Nav.Link as="a" className="text-light text-center" role="button">
							<FontAwesomeIcon className="mr-2" icon={faMicrochip} />
							About Me
						</Nav.Link>
					</Link>
					<Link href="/projects" passHref>
						<Nav.Link as="a" className="nav-link text-light text-center" role="button">
							<FontAwesomeIcon className="mr-2" icon={faLaptopCode} />
							Projects
						</Nav.Link>
					</Link>
					<Link href="/education" passHref>
						<Nav.Link as="a" className="nav-link text-light text-center" role="button">
							<FontAwesomeIcon className="mr-2" icon={faGraduationCap} />
							Education
						</Nav.Link>
					</Link>
					<Link href="/work" passHref>
						<Nav.Link as="a" className="nav-link text-light text-center" role="button">
							<FontAwesomeIcon className="mr-2" icon={faBriefcase} />
							Work Experience
						</Nav.Link>
					</Link>
					<Link href="/contact" passHref>
						<Nav.Link as="a" className="nav-link text-light text-center" role="button">
							<FontAwesomeIcon className="mr-2" icon={faMobileAlt} />
							Contact Me
						</Nav.Link>
					</Link>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}