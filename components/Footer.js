import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import dynamic from 'next/dynamic';
const FacebookLink = dynamic(() => import('../components/FacebookLink'));
const LinkedInLink = dynamic(() => import('../components/LinkedInLink'));
const GitLabLink = dynamic(() => import('../components/GitLabLink'));

export default function Footer() {
	return(
		<div className="text-light footer d-flex justify-content-center align-items-center fixed-bottom">
			<a 
				href="https://www.facebook.com/XeiyaShin/" 
				target="_blank" 
				className="text-light mx-4"
			>
				<FacebookLink />
			</a>
			<a 
				href="https://www.linkedin.com/in/arvin-josef-lacdao-453209196/" 
				target="_blank" 
				className="text-light mx-4"
			>
				<LinkedInLink />
			</a>
			<a 
				href="https://gitlab.com/xeiya935" 
				target="_blank" 
				className="text-light mx-4"
			>
				<GitLabLink />
			</a>
			<Link href="/contact">
				<a className="text-light">
					<FontAwesomeIcon className="footer-link mx-4 footer-yellow" icon={faEnvelope} />
				</a>
			</Link>
			<p className="d-inline"></p>
		</div>
	)
}