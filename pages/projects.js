import React from 'react';
import Head from 'next/head';

export default function Projects() {
	return(
		<React.Fragment>
			<Head>
				<title>Projects</title>
			</Head>
			<div className="background center flex-column page-container">
				<div className="header-container position-relative">
					<div className="center">
						<p className="header-glitch-1">Projects</p>
						<p className="header-glitch-2">Projects</p>
						<p className="header-glitch-3">Projects</p>
					</div>
					<hr className="header-hr project-hr"/>
				</div>
				<div className="projects-container center pt-2">
					<div className="projects-card center flex-column text-center">
						<h4>Herbs</h4>
						<a href="https://xeiya935.gitlab.io/capstone-1/" target="_blank">
							<img className="projects-thumbnail" src="/capstone1.png" />
						</a>
						<div className="sub-header-container position-relative">
							<div className="center">
								<p className="header-glitch-1 sub-header">HTML | CSS</p>
								<p className="header-glitch-2 sub-header">HTML | CSS</p>
								<p className="header-glitch-3 sub-header">HTML | CSS</p>
							</div>
						</div>
						<div className="projects-text">
							<p>
								A frontend e-commerce website that helps promote health where users can purchase different medicinal herbs.
							</p>
						</div>
					</div>
					<div className="projects-card center flex-column text-center">
						<h4>Codebase</h4>
						<a href="https://codebase935.netlify.com/#/" target="_blank">
							<img className="projects-thumbnail" src="/capstone3.jpg" />
						</a>
						<div className="sub-header-container position-relative">
							<div className="center">
								<p className="header-glitch-1 sub-header">MongoDB | Express JS | React JS | Node JS</p>
								<p className="header-glitch-2 sub-header">MongoDB | Express JS | React JS | Node JS</p>
								<p className="header-glitch-3 sub-header">MongoDB | Express JS | React JS | Node JS</p>
							</div>
						</div>
						<div className="projects-text">
							<p>
								A booking system where users can book for online programming classes.
							</p>
						</div>
					</div>
				</div>
			</div>
		</React.Fragment>
	)
}