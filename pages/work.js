import React from 'react';
import Head from 'next/head';

export default function Work() {
	return(
		<React.Fragment>
			<Head>
				<title>Work</title>
			</Head>
			<div className="background center flex-column page-container">
				<div className="header-container position-relative">
					<div className="center">
						<p className="header-glitch-1">Work Experience</p>
						<p className="header-glitch-2">Work Experience</p>
						<p className="header-glitch-3">Work Experience</p>
					</div>
					<hr className="header-hr work-hr"/>
				</div>
				<div className="works-container">
					<div className="works-card text-center">
						<h4>IT Instructor</h4>
						<h5>Zuitt</h5>
						<p className="sub-header m-0">Caswynn Building, Quezon City</p>
						<p className="sub-header">(Jan 2020 - Present)</p>
						<a href="https://zuitt.co/" target="_blank">
							<img className="card-img-thumbnail" src="/zuitt.png"/>
						</a>
						<p className="pt-3">
							Instructs actual and online classes teaching a group of students on how to develop web applications.
						</p>
						<p>
							Conducts technical trainings to other instructors.
						</p>
						<p>
							Prepare slides, applications and other materials needed for daily class schedules.
						</p>
						<p>
							Undergoes further trainings to learn more technologies.
						</p>
					</div>
					<div className="works-card text-center">
						<h4>Collections Associate</h4>
						<h5>HSBC</h5>
						<p className="sub-header m-0">Northgate Cyberzone, Muntinlupa City</p>
						<p className="sub-header">(Nov 2009 - May 2011)</p>
						<a href="https://www.hsbc.com.ph/" target="_blank">
							<img className="card-img-thumbnail" src="/hsbc.png"/>
						</a>
						<p className="pt-3">
							Completed multiple outbound calls to US and Canadian credit card holders.
						</p>
						<p>
							Educated clients about their financial status and suggested payment methods to maintain their credit limit.
						</p>
						<p>
							Provided best solutions for various customer concerns about billing.
						</p>
					</div>
					<div className="works-card text-center">
						<h4>Customer Service Representative</h4>
						<h5>Teleperformance</h5>
						<p className="sub-header m-0">Santana Grove, Paranaque City</p>
						<p className="sub-header">(Dec 2008 - Mar 2009)</p>
						<a href="https://www.teleperformance.com/en-us" target="_blank">
							<img className="card-img-thumbnail" src="/teleperformance.png"/>
						</a>
						<p className="pt-3">
							Managed a variety of inbound calls from US clients seeking assistance for hotel bookings.
						</p>
						<p>
							Supplied necessary information for customer's hotel preferences such as their desired location, amenities, etc.
						</p>
						<p>
							Assisted customers to complete and pay for their hotel reservations and other related travel services.
						</p>
					</div>
				</div>
			</div>
		</React.Fragment>
	)
}