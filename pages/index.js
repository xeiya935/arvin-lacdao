import React from 'react';
import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function Home() {
	return (
		<React.Fragment>
			<div className="bg-container background center flex-column">
				<h1>Arvin Lacdao</h1>
				<hr className="mb-5"/>
				<div className="center">
					<p className="title-glitch-1">Full Stack Web Developer</p>
					<p className="title-glitch-2">Full Stack Web Developer</p>
					<p className="title-glitch-3">Full Stack Web Developer</p>
				</div>
			</div>
		</React.Fragment>
	)
}
