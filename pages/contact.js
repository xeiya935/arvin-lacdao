import React, { useState, useEffect } from 'react';
import Head from 'next/head';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function Work() {

	const [name, setName] = useState("");
	const [email, setEmail] = useState("");
	const [inquiry, setInquiry] = useState("");
	const [message, setMessage] = useState("");
	const [isActive, setIsActive] = useState(false); 

	function sendMail(e) {

		e.preventDefault()

		fetch(`${process.env.API_URL}/api/users/contact`, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json '},
			body: JSON.stringify({
				name: name,
				email: email,
				inquiry: inquiry,
				message: message
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data.accepted !== null || data.accepted !== undefined) {
	            Swal.fire('Email Sent', `I'll get back to you as soon as I can.`, 'success')
	        } else {
	            Swal.fire('Oops!', 'Something went wrong! Try again later.', 'error')
	        }

		})

		setName("");
		setEmail("");
		setInquiry("");
		setMessage("");
		setIsActive(false);

	}

	useEffect(() => {

		if (name !== "" && email !== "" && inquiry !== "" && message !== "") {
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [name, email, message])

	return(
		<React.Fragment>
			<Head>
				<title>Contact</title>
			</Head>
			<div className="background center flex-column page-container">
				<div className="header-container position-relative">
					<div className="center">
						<p className="header-glitch-1">Contact</p>
						<p className="header-glitch-2">Contact</p>
						<p className="header-glitch-3">Contact</p>
					</div>
					<hr className="header-hr contact-hr"/>
				</div>
				<Form onSubmit={(e) => sendMail(e)} className="contact-container pt-3">

				    <Form.Group controlId="name">
				        <Form.Label>Name</Form.Label>
				        <Form.Control 
				            type="text" 
				            value={name} 
				            onChange={e => setName(e.target.value)} 
				            required
				        />
				    </Form.Group>

				    <Form.Group controlId="email">
				        <Form.Label>Email Address</Form.Label>
				        <Form.Control 
				            type="text" 
				            value={email} 
				            onChange={e => setEmail(e.target.value)} 
				            required
				        />
				    </Form.Group>

				    <Form.Group controlId="inquiry">
				        <Form.Label>Inquiry</Form.Label>
				        <Form.Control 
				            type="text" 
				            value={inquiry} 
				            onChange={e => setInquiry(e.target.value)} 
				            required
				        />
				    </Form.Group>

				    <Form.Group controlId="message">
				        <Form.Label>Message</Form.Label>
				        <Form.Control 
				            as="textarea" 
				            rows="5"  
				            value={message} 
				            onChange={e => setMessage(e.target.value)} 
				            required
				        />
				    </Form.Group>

				    <div className="center">
				    	{ isActive
				    		?
				    			<Button className="contact-btn" type="submit">Submit</Button>
				    		:
				    			<Button className="contact-btn" type="submit" disabled>Submit</Button>
				    	}
				    </div>

				    <div className="contact-card text-center mt-3">
				    	<p className="mb-0">Email</p>
				    	<p>arvinjosefblacdao@gmail.com</p>
				    	<p className="mb-0">Phone</p>
				    	<p>+63 977 706 5589</p>
				    </div>

				</Form>
			</div>
		</React.Fragment>
	)
}